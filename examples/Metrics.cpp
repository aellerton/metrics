// Copyright (c) Performance Does Matter. All Rights Reserved.

#include <iostream>
#include <random>
#include <vector>
#include "metrics/Metrics.h"
#include <ctime>

struct timer {
    std::clock_t start = std::clock();

    ~timer() {
        auto duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
        std::cout << "duration: " << duration << " seconds\n";
    }
};

template< typename T >
void createRandomData( std::vector<T>& data, T min, T max)
{
    timer _;
    std::cout << "Creating random data..." << std::endl;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(min, max);

    for (int i = 0; i < data.size(); ++i) {
        data[i] = dis(gen);
    }
}

int main() {
    const int COUNT = 1000*1000;
    std::vector<int> data(COUNT);

    createRandomData(data, 0, 100);

    const std::int64_t N = 256;

    Metrics metrics;
    Counter<int> c1;
    Counter<int> c2;
    MovingAverage<int, N, N, N, N> ma;
    MovingAverage<float, N, N, N, N> ma2;

    metrics.add("Counter 1", c1);
    metrics.add("Counter 2", c2);
    metrics.add("Moving Average (int)", ma);
    metrics.add("Moving Average (float)", ma2);

    for (int i = 0; i < 100; ++i) { c1.inc(); }
    for (int i = 0; i < 200; ++i) { c2.inc(); }

    std::cout << "Streaming data to moving average calculator..." << std::endl;
    {
        timer _;
        for (int i : data) {
            ma.push(i);
        }
    }

    std::cout << "Streaming data to moving average calculator..." << std::endl;
    {
        timer _;
        for (int i : data) {
            ma2.push(i);
        }
    }

    metrics.report();
}
