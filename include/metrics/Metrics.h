// Copyright (c) Performance Does Matter. All Rights Reserved.

#pragma once

#ifdef _WIN32
#include <malloc.h>
#else
#include <cstdlib>
#include <cstring>
#endif
#include <atomic>
#include <string>
#include <sstream>
#include <map>
#include <functional>

//
// https://metrics.dropwizard.io/3.1.0/getting-started/
//
// Meter
// Gauge
// Histogram
// Timers
// Health Checks
//

template< typename T >
class Counter {
    std::atomic<T> counter_;
public:
    Counter(T initVal = 0) : counter_(initVal) {}
    void inc() { counter_++; }
    void dec() { counter_--; }
    T get() const { return counter_; }
};


template< typename T>
T round(float val) {
    return (T)val;
}

template<>
int round<int>(float val) {
    return (int)std::round( val );
}


template< typename T, int... X>
class MovingAverage {
public:
    void push(T val) {}
    T avg() const { return 0; }
    void get(std::vector<T>& vec) const {}
};

template< typename T, int N, int... M>
class MovingAverage<T, N, M... > {
    static const inline int BUFLEN = N;
    static const inline int BUFLEN_MINUS1 = BUFLEN - 1;
    static const inline float ONE_ON_BUFFLEN = 1.0f / BUFLEN;

#ifdef _WIN32
    // TODO: validate if C++17 in MSVC supports the conventional API
    T* buf = (T*)_aligned_malloc(BUFLEN * sizeof(T), 64);
#else
    T* buf = (T*)std::aligned_alloc(64, BUFLEN * sizeof(T));
#endif
    std::uint64_t p = 0;

    MovingAverage< T, M... > next;
public:
    MovingAverage() {
        memset(buf, 0, BUFLEN * sizeof(T));
    }

    ~MovingAverage() {
#ifdef _WIN32
    // TODO: validate if C++17 in MSVC supports the conventional API
        _aligned_free(buf);
#else
        std::free(buf);
#endif
    }

    std::uint64_t idx(std::uint64_t i) {
        return i & BUFLEN_MINUS1;
    }

    void push(T val) {
        auto i = idx(p++);
        buf[i] = val;
        if (i == BUFLEN_MINUS1) {
            p = BUFLEN;  // reset p to BUFLEN to avoid overflow - don't reset to 0 as a non-full buffer is a special case
            next.push(fast_avg());
        }
    }

    T avg() const {
        if (p) {
            return fast_avg();
        }
        return 0;
    }

    T fast_avg() const {
        T avg = T(0);
        for (int i = 0; i < BUFLEN; ++i) {
            avg += buf[i];
        }
        return round<T>(((p < BUFLEN) ? avg / (float)p : avg * ONE_ON_BUFFLEN));
    }

    void get(std::vector<T>& vec) const {
        vec.emplace_back(avg());
        next.get(vec);
    }

    std::vector<T> get() const 
    { 
        std::vector<T> vec;
        get(vec);
        return vec; 
    }
};

namespace std {
    template< typename T >
    inline std::string to_string(const std::vector<T>& vec) {
        std::stringstream ss;
        bool first = true;
        for ( auto i : vec)
        {
            if (!first)
                ss << ", ";
            ss << std::to_string(i);
            first = false;
        }
        return ss.str();
    }
}

class MetricReporter
{
    template< typename T >
    std::string toStringTyped()
    {
        T* metric = (T*)metric_;
        return std::to_string(metric->get());
    }

    std::string toStringDummy()
    {
        return "not bound";
    }

    void* metric_;
    typedef std::string (MetricReporter::*FN)();
    FN toString_;

public:
    MetricReporter() 
    {
        toString_ = &MetricReporter::toStringDummy;
    }

    template< typename T >
    MetricReporter(const T& metric)
    {
        metric_ = (void*)(&metric);
        toString_ = &MetricReporter::toStringTyped<T>;
    }
    
    std::string toString()
    {
        return (this->*toString_)();
    }
};

class Metrics
{
    std::map< std::string, MetricReporter > metrics_;
public:
    
    template< typename T >
    void add(const std::string& name, const T& metric) {
        metrics_[name] = MetricReporter(metric);
    }

    void report() 
    {
        for (auto& r : metrics_)
        {
            std::cout << r.first << ": " << r.second.toString() << std::endl;
        }
    }
};