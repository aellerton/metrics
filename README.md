# PerformanceMetrics

High performance C++ metrics library

## Build on Linux

With [gn][1]:

    # generate ninja files to directory called "out/gn-based"
    # (name directory as you prefer)
    gn gen out/gn-debug

    # invoke ninja in that directory
    ninja -C out/gn-debug

    # run the sample
    ./out/gn-debug/example-1

With cmake:

    # create out-of-source build area - name the directory as you prefer
    mkdir out out/cmake-debug

    # configure the build
    # (you can choose something other than Ninja)
    cd out/cmake-debug
    cmake -G Ninja ../..

    # build...
    ninja

[1]: https://chromium.googlesource.com/chromium/src/tools/gn/+/48062805e19b4697c5fbd926dc649c78b6aaa138/README.md
